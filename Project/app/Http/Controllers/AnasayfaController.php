<?php

namespace App\Http\Controllers;

use App\Model\Kategori;
use Illuminate\Http\Request;

class AnasayfaController extends Controller
{
    public function index(){
        $kategoriler=Kategori::whereRaw('ust_id is null')->take(8)->get();
        return view('anasayfa', compact('kategoriler'));
    }

   /* public function index(){
        $isim="ayşe";
        $soyisim="Akbulut";
        $isimler=['Ali', 'Veli', 'Hakkı', 'Rıza'];
        /*return view('anasayfa', ['isim'=>'ayse']);*/
       // return view('anasayfa', compact('isim', 'soyisim', 'isimler'));
        /*return view('welcome')->with(['isim'=>$isim, 'soyisim'=>'$soyisim']);*/
    //}*/
}
