<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'AnasayfaController@index')->name('kategori');

Route::get('kategori/{slug_kategoriadi}', 'KategoriController@index')->name('kategori');

Route::get('/urun/{slug_urunadi}', 'UrunController@index')->name('urun');

Route::get('/sepet/{slug_sepetadi}', 'SepetController@index')->name('sepet');

Route::get('/odeme', 'OdemeController@index')->name('odeme');

Route::get('/siparis', 'SiparisController@index')->name('siparis');

Route::get('/siparis', 'SiparisController@index')->name('siparisler');

Route::get('/siparis/{id}', 'SiparisController@detay')->name('siparis');

Route::group(['prefix'=> 'kullanici'], function(){

Route::get('/oturumac', 'KullaniciController@giris_form')->name('kullanici.oturumac');

Route::get('/kaydol', 'KullaniciController@kaydol_form')->name('kullanici.kaydol');

});




Route::get('/urun/{isim}/{id?}', function ($urunadi, $id=0){
    return "Ürün Adı: $id $urunadi";
})->name('urun detay');

Route::get('/kampanya', function (){
    return redirect()->route('urun detay', ['urunadi'=>'elma', 'id'=>5]);
});

Route::view('/kategori', 'kategori');
Route::view('/urun', 'urun');
Route::view('/sepet', 'sepet');
