@extends('layouts.master')
@section('title', 'Kategori')
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{route('anasayfa')}}">Anasayfa</a></li>
            <li class="active">{{$kategori->kategori_adi}}</li>
        </ol>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$kategori->kategori_adi}}</div>
                    <div class="panel-body">
                    <h3>Alt Kategoriler</h3>
                        <hr>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="products bg-content">

                    Sırala
                    <a href="?order=coksatanlar" class="btn btn-default">Çok Satanlar</a>
                    <a href="?order=yeni" class="btn btn-default">Yeni Ürünler</a>
                    <hr>

                <div class="row">

                        <div class="col-md-12">Bu kategoride henüz ürün bulunmamaktadır!</div>

                        <div class="col-md-4 product">
                            <a href="">
                                <img src="">
                            </a>

                        </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
