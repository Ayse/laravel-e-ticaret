@extends('layouts.master')
@section('title', 'Sipariş Detayı')
@section('content')
    <div class="container">
        <div class="bg-content">
            <a href="" class="btn btn-xs btn-primary">
                <i class="glyphicon glyphicon-arrow-left"></i> Siparişlere Dön
            </a>
            <h2>Sipariş </h2>
            <table class="table table-bordererd table-hover">
                <tr>
                    <th colspan="2">Ürün</th>
                    <th>Tutar</th>
                    <th>Adet</th>
                    <th>Ara Toplam</th>
                    <th>Durum</th>
                </tr>
                    <tr>
                        <td style="width:120px">
                            <a href="">
                                <img src="" style="height: 120px;">
                            </a>
                        </td>
                        <td>
                            <a href="">

                            </a>
                        </td>

                    </tr>

                <tr>
                    <th colspan="4" class="text-right">Toplam Tutar</th>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <th colspan="4" class="text-right">Toplam Tutar (KDV'li)</th>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <th colspan="4" class="text-right">Sipariş Durumu</th>
                    <td colspan="2"></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
