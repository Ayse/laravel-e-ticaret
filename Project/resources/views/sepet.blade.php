@extends('layouts.master')
@section('title', 'Sepet')
@section('content')
    <div class="container">
        <div class="bg-content">
            <h2>Sepet</h2>

                <table class="table table-bordererd table-hover">
                    <tr>
                        <th colspan="2">Ürün</th>
                        <th>Adet Fiyatı</th>
                        <th>Adet</th>
                        <th>Tutar</th>
                    </tr>

                        <tr>
                            <td style="width: 120px;">
                                <a href="">
                                    <img src="http://lorempixel.com/60/60/food/2">
                                </a>
                            </td>
                            <td>
                                <a href="">

                                </a>
                                <form action="" method="post">

                                    <input type="submit" class="btn btn-danger btn-xs" value="Sepetten Kaldır">
                                </form>
                            </td>
                            <td> ₺</td>
                            <td>
                                <a href="#" class="btn btn-xs btn-default urun-adet-azalt" data-id="">-</a>
                                <span style="padding: 10px 20px"></span>
                                <a href="#" class="btn btn-xs btn-default urun-adet-artir" data-id="">+</a>
                            </td>
                            <td class="text-right">

                            </td>
                        </tr>
                   <tr>
                        <th colspan="4" class="text-right">Alt Toplam</th>
                        <td class="text-right"> ₺</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right">KDV</th>
                        <td class="text-right"> ₺</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right">Genel Toplam</th>
                        <td class="text-right"> ₺</td>
                    </tr>
                </table>

                <form action="" method="post">

                    <input type="submit" class="btn btn-info pull-left" value="Sepeti Boşalt">
                </form>
                <a href="" class="btn btn-success pull-right btn-lg">Ödeme Yap</a>

        </div>
    </div>
@endsection

