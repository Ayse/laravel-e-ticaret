@extends('layouts.master')
@section('title', 'urun')
@section('content')
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="">Anasayfa</a></li>

                <li><a href=""></a></li>

            <li class="active"></li>
        </ol>
        <div class="bg-content">
            <div class="row">
                <div class="col-md-5">
                    <img src="" class="img-responsive">
                    <hr>
                    <div class="row">
                        <div class="col-xs-3">
                            <a href="#" class="thumbnail"><img src="http://lorempixel.com/60/60/food/2"></a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#" class="thumbnail"><img src="http://lorempixel.com/60/60/food/3"></a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#" class="thumbnail"><img src="http://lorempixel.com/60/60/food/4"></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <h1></h1>
                    <p class="price"></p>
                    <form action="" method="post">

                        <select name="adet" class="form-control pull-left" style="width: 100px; margin-right: 10px;">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                        <input type="hidden" name="id" value="">
                        <input type="submit" class="btn btn-theme" value="Sepete Ekle">
                    </form>
                </div>
            </div>

            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#t1" data-toggle="tab">Ürün Açıklaması</a></li>
                    <li role="presentation"><a href="#t2" data-toggle="tab">Yorumlar</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="t1">
                    </div>
                    <div role="tabpanel" class="tab-pane" id="t2">
                        Henüz yorum yapılmadı!
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
